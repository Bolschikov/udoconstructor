/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ws2812.h"
#include "control_light.h"
#include "math.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define NUM_FULL_TICKS 		15
#define FULL_TICK			6  //count of interruptions
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
uint8_t flagStopDma = 1;

uint8_t flagSignal = 0, flagDeepDelay = 0, numStartCount = 0, numIter = 0, numIterTim = 0;
uint32_t lastLvlResist = 0, curLvlResist = 0, cntIntrTIM4 = 0;
uint32_t lastUadc = 200, curUadc, bufLastUadc;
uint8_t min, hour;
uint8_t isWorking = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
//void switchOffAllLeds();
void stopTimer(void);
uint32_t getResistance(uint32_t uADC, uint8_t* i);
void resistForTransmit(uint8_t* scale, uint32_t* value, uint8_t* arr);
void controlLight();
void fillColor(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_tim2_ch1;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart2;
/* USER CODE BEGIN EV */
extern uint8_t red, green, blue, flagLightOn, numModes;
extern uint16_t BUF_DMA [ARRAY_LEN];
extern ADC_HandleTypeDef hadc1;
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 channel5 global interrupt.
  */
void DMA1_Channel5_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel5_IRQn 0 */
	  HAL_TIM_PWM_Stop_DMA(&htim2,TIM_CHANNEL_1);

  /* USER CODE END DMA1_Channel5_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_tim2_ch1);
  /* USER CODE BEGIN DMA1_Channel5_IRQn 1 */
  flagStopDma = 1;
  /* USER CODE END DMA1_Channel5_IRQn 1 */
}

/**
  * @brief This function handles EXTI line[9:5] interrupts.
  */
void EXTI9_5_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI9_5_IRQn 0 */

  /* USER CODE END EXTI9_5_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
  /* USER CODE BEGIN EXTI9_5_IRQn 1 */

//  lastIdMotion = curIdMotion;
    switch(numModes){
	  case 1:
		  if(cntIntrTIM4 == 0){
			  switchOnLeds(red, green, blue);
			  TIM4->CNT = 0;
			  HAL_TIM_Base_Start(&htim4);
			  HAL_TIM_Base_Start_IT(&htim4);
		  }
		  break;
	  case 2:
		  if(cntIntrTIM4 == 0){
			  	TIM4->CNT = 0;
				TIM3->CCR1 = 50000;
				HAL_TIM_Base_Start(&htim4);
				HAL_TIM_Base_Start_IT(&htim4);
				switchOnLeds(RED, NO_COLOR, NO_COLOR);
		  }
//		  TIM4->CNT = 0;
//		  TIM3->CCR1 = 51000;
//		  HAL_TIM_Base_Start(&htim4);
//		  HAL_TIM_Base_Start_IT(&htim4);
		  break;
    }


  /* USER CODE END EXTI9_5_IRQn 1 */
}

/**
  * @brief This function handles TIM1 update interrupt.
  */
void TIM1_UP_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_IRQn 0 */

	getTime(&min, &hour);
	sendTime(min, 0);
	sendTime(hour, 2);

  /* USER CODE END TIM1_UP_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_IRQn 1 */

  /* USER CODE END TIM1_UP_IRQn 1 */
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */

  /* USER CODE END TIM2_IRQn 0 */
  HAL_TIM_IRQHandler(&htim2);
  /* USER CODE BEGIN TIM2_IRQn 1 */

  /* USER CODE END TIM2_IRQn 1 */
}

/**
  * @brief This function handles TIM4 global interrupt.
  */
void TIM4_IRQHandler(void)
{
  /* USER CODE BEGIN TIM4_IRQn 0 */

  /* USER CODE END TIM4_IRQn 0 */
  HAL_TIM_IRQHandler(&htim4);
  /* USER CODE BEGIN TIM4_IRQn 1 */

  switch(numModes){
	  case 1:
		  if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5) == GPIO_PIN_RESET)
			  stopTimer();
		  else
			  cntIntrTIM4++;
		  break;
	  case 2:
		  if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5) != GPIO_PIN_RESET){
			  HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);
		    if(cntIntrTIM4 % 2 == 0)
		    	switchOnLeds(NO_COLOR, NO_COLOR, BLUE);
		  	else
		  		switchOnLeds(RED, NO_COLOR, NO_COLOR);
		    cntIntrTIM4++;
		  } else{
			  stopTimer();
			  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
			  TIM3->CCR1 = 65000;
		  }
		  break;
	  case 3:
			if(flagStopDma & isWorking){
				HAL_TIM_PWM_Start_DMA(&htim2, TIM_CHANNEL_1, (uint32_t*)&BUF_DMA, ARRAY_LEN);
				flagStopDma = 0;
			}
		  if(!isWorking){
			  isWorking = 1;
			  curUadc = 0;
			  for(uint8_t i = 0; i < 10; i++){
				  HAL_ADC_Start(&hadc1);
				  HAL_ADC_PollForConversion(&hadc1, 100);
				  curUadc += (uint32_t) HAL_ADC_GetValue(&hadc1);
				  HAL_ADC_Stop(&hadc1);
			  }
			  curUadc /= 10;

		  }
		  if(isWorking)
			  controlLight();
		  break;
	  case 7:
			 initRainbow(&numIter);
			  sendToLed();
	 		  numIter++;
		  break;
	  case 11:
		  HAL_ADC_Start(&hadc1);
		  HAL_ADC_PollForConversion(&hadc1, 100);
		  uint32_t uADC = (uint32_t) HAL_ADC_GetValue(&hadc1);

		  HAL_ADC_Stop(&hadc1);
//		  uint32_t resist = uADC * R1 / ((U * 1000) - uADC);
		  uint8_t arrTransmitData[] = {1, 0, 0, 0, 0};
		  for(uint8_t i = 1; i < sizeof(arrTransmitData); i++){
			  arrTransmitData[i] = uADC % 10;
			  uADC = uADC / 10;
		  }
		  HAL_UART_Transmit(&huart2, arrTransmitData, sizeof(arrTransmitData), 100);
		  break;
  }


  /* USER CODE END TIM4_IRQn 1 */
}

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

/* USER CODE BEGIN 1 */

void stopTimer(void){
	  HAL_TIM_Base_Stop(&htim4);
	  HAL_TIM_Base_Stop_IT(&htim4);
	  cntIntrTIM4 = 0;
	  switchOffAllLeds();
	  return;
}


//uint32_t getResistance(uint32_t uADC, uint8_t* i){
////	  HAL_ADC_Start(&hadc1);
////	  HAL_ADC_PollForConversion(&hadc1, 100);
////	  uint32_t uADC = (uint32_t) HAL_ADC_GetValue(&hadc1) / 1000;
////	  HAL_ADC_Stop(&hadc1);
////	  if(uADC > 3.2) uADC = 3.2;
////	  HAL_ADC_Stop(&hadc1);
//
//	  uint32_t resist = uADC * R1 / ((U * 1000) - uADC);
//	  uint32_t bufResist = resist;
////	  uint8_t i = 0;
//	  while(bufResist){
//		  bufResist = bufResist % 10;
//		  (*i)++;
//
//	  }
//	  if((*i) < 1 || (*i) > 6){
//		  resist = 999;
//	  }
//	  printf("HUI");
//	  return resist;
//}

void resistForTransmit(uint8_t* scale, uint32_t* value, uint8_t* arr){
	uint8_t val = (*value) / (*scale);
	for(uint8_t i = 2; i  > -1; i--){
		arr[i] = val % 10;
	}

}


void controlLight(){
	float cur_last;
	cur_last = (float)((float) curUadc / (float)lastUadc);
	if(cur_last > 1.2){
		fillColor();
		bufLastUadc += 30;
		if(bufLastUadc >= curUadc){
			isWorking = 0;
			lastUadc = curUadc;
			bufLastUadc = curUadc;
		}
	} else if(cur_last < 0.8){
		fillColor();
		bufLastUadc -= 30;
		if(bufLastUadc <= curUadc){
			isWorking = 0;
			lastUadc = curUadc;
			bufLastUadc = curUadc;
		}
	} else
		isWorking = 0;
}

void fillColor(){
	uint16_t color = 0;
	uint8_t green = 0;
	uint8_t blue = 0;
	if((bufLastUadc / 10) * (bufLastUadc / 10) / 100 > 200)
		color = ((bufLastUadc / 10) * (bufLastUadc / 10)) / 100  - 200;
	else
		color = 0;
	if (color > 254){
		color = 254;
		lastUadc = 3100;
	}

	ws2812_pixel_rgb_to_buf_dma(&color, &green, &blue);
}


/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
