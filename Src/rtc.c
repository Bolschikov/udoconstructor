#include "rtc.h"
#include "i2c.h"
extern I2C_HandleTypeDef hi2c2;
uint8_t RTC_ConvertFromDec(uint8_t c)
{
	uint8_t ch = ((c>>4)*10+(0x0F&c));
	return ch;
}

uint8_t RTC_ConvertFromBinDec(uint8_t c)
{
	uint8_t ch = ((c/10)<<4)|(c%10);
	return ch;
}

//void setTimeRtc(uint8_t min, uint8_t hours){
////	aTxBuffer[0] = 0;
////	I2C_WriteBuffer(hi2c2,(uint16_t)0xD0,1);//Ïåðåäàäèì àäðåñ óñòðîéñòâó
//	uint8_t initHour = 16;
//	aTxBuffer[0] = 0x03;
//	aTxBuffer[1] = RTC_ConvertFromBinDec(initHour);
//	I2C_WriteBuffer(hi2c2,(uint16_t)0xD0,2);
//	while(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
//	{
//	}
//}

void sendTimeRtc(uint8_t addr, uint8_t time){
	aTxBuffer[0] = addr;
	aTxBuffer[1] = RTC_ConvertFromBinDec(time);
	I2C_WriteBuffer(hi2c2,(uint16_t)ADDR_RTC, 2);
}

void sendNull(){
	aTxBuffer[0] = 0;
	I2C_WriteBuffer(hi2c2, (uint16_t)ADDR_RTC, 1);//Ïåðåäàäèì àäðåñ óñòðîéñòâó
}

void getTime(uint8_t* min, uint8_t* hour){
	sendNull();
	I2C_ReadBuffer(hi2c2,(uint16_t)ADDR_RTC | READ_BIT_RTC, 7);
	(*min) = rBuffer[1];
	(*min) = RTC_ConvertFromDec((*min));
	(*hour) = rBuffer[2];
	(*hour) = RTC_ConvertFromDec((*hour));
}
