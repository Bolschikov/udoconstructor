#include "i2c.h"

#include "ht16k33.h"
extern I2C_HandleTypeDef hi2c1;
const uint8_t numeric7seg[] = {
	0x3F,	//0
	0x06,	//1
	0x5B,	//2
	0x4F,	//3
	0x66,	//4
	0x6D,	//5
	0x7D,	//6
	0x07,	//7
	0x7F,	//8
	0x6F	//9
};

const uint8_t addr7seg[] = {
		0x00,	//1
		0x02,	//2
		0x04,	//3
		0x06	//4
};

const uint8_t numbers7seg = 4;
uint8_t lastMinSecDecade = 0, lastHourFstDecade = 0, lastHourSecDecade = 0;
uint8_t lastTime[4] = {255, 255, 255, 255};  //0, 1 - mins; 2,3 - hours



void init7seg(){
	aTxBuffer[0] = 0x21;
	I2C_WriteBuffer(hi2c1,(uint16_t)0xE0,1);
	aTxBuffer[0] = 0x81;
	I2C_WriteBuffer(hi2c1,(uint16_t)0xE0,1);
	clear7seg();
}

void clear7seg(){
	for(uint8_t i = 0; i < numbers7seg; i++){
		aTxBuffer[0] = addr7seg[i];
		aTxBuffer[1] = CLEARBYTE;
		I2C_WriteBuffer(hi2c1, ADDR_HT, 2);
	}
}

void sendTime(uint8_t time, uint8_t item7seg){
	uint8_t decade = getDecade(&time);
	if(!needUpdate(item7seg, decade)) return;
	decade = getDecade(&time);
	if(!needUpdate(item7seg + 1, decade)) return;
}

void sendNumeric(uint8_t item7seg, uint8_t itemNum){
	if(item7seg == 2)
		aTxBuffer[1] = numeric7seg[itemNum] | DOT;
	else
		aTxBuffer[1] = numeric7seg[itemNum];
	aTxBuffer[0] = addr7seg[item7seg];
//	aTxBuffer[1] = numeric7seg[itemNum];
	I2C_WriteBuffer(hi2c1, ADDR_HT, 2);
}

uint8_t getDecade(uint8_t* time){
	uint8_t decade = (*time) % 10;
	(*time) /= 10;
	return decade;
}

uint8_t needUpdate(uint8_t item7seg, uint8_t decade){
	if(lastTime[item7seg] != decade){
		sendNumeric(item7seg, decade);
		lastTime[item7seg] = decade;
		return 1;
	}
	else return 0;
}


