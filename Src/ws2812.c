/*
/*
 * ws2812.c
 *
 *  Created on: 26 ����. 2018 �.
 *      Author: annsi
 */


/*
 * ws2812.c
 *
 *  Created on: 10 ����. 2018 �.
 *      Author: annsi
 */


#include "ws2812.h"
#include "stm32f1xx_hal.h"
//----------------------------------------------------------------------------

extern TIM_HandleTypeDef htim2;
uint8_t flagStopDma;
//const uint8_t numColros = 16;
uint8_t arr_colors [COLORNUMBER][3] = {{255, 0, 0},{0,255, 0},{0, 0, 255}};
//#define COLORNUMBER 2
//uint8_t arr_colors [COLORNUMBER][2] = {{255, 0, 0},{0,255, 0}};
uint8_t dir = 0;
uint8_t redSide=0;
uint8_t blueSide=0;
//----------------------------------------------------------------------------

uint16_t BUF_DMA [ARRAY_LEN] = {0};

void ws2812_init(void){
  for(int i = DELAY_LEN; i < ARRAY_LEN; i++) BUF_DMA[i] = LOW;
}

void ws2812_pixel_rgb_to_buf_dma(uint8_t* Rpixel, uint8_t* Gpixel, uint8_t* Bpixel){
//  volatile uint16_t i;
  for(uint8_t posX = 0; posX < LED_COUNT; posX++){
	  for(uint8_t i = 0; i < 8; i++)
	  {
		if (BitIsSet(*Rpixel, (7 - i)))
			BUF_DMA[DELAY_LEN + posX * 24 + i + 8] = HIGH;
		else
			BUF_DMA[DELAY_LEN + posX * 24 + i + 8] = LOW;
		if (BitIsSet(*Gpixel, (7 - i)))
		  BUF_DMA[DELAY_LEN + posX * 24 + i + 0] = HIGH;
		else
		  BUF_DMA[DELAY_LEN + posX * 24 + i + 0] = LOW;
		if (BitIsSet(*Bpixel, (7 - i)))
		  BUF_DMA[DELAY_LEN + posX * 24 + i + 16] = HIGH;
		else
		  BUF_DMA[DELAY_LEN + posX * 24 + i + 16] = LOW;
	  }
  }

}
//
//void fillBuffDma(uint8_t* Rpixel, uint8_t* Gpixel, uint8_t* Bpixel, uint8_t* numPix){
//	  for(uint8_t i = 0; i < 8; i++)
//	  {
//		if (BitIsSet(*Rpixel, (7 - i)))
//			BUF_DMA[DELAY_LEN + (*numPix) * 24 + i + 8] = HIGH;
//		else
//			BUF_DMA[DELAY_LEN + (*numPix) * 24 + i + 8] = LOW;
//		if (BitIsSet(*Gpixel, (7 - i)))
//		  BUF_DMA[DELAY_LEN + (*numPix) * 24 + i + 0] = HIGH;
//		else
//		  BUF_DMA[DELAY_LEN + (*numPix) * 24 + i + 0] = LOW;
//		if (BitIsSet(*Bpixel, (7 - i)))
//		  BUF_DMA[DELAY_LEN + (*numPix) * 24 + i + 16] = HIGH;
//		else
//		  BUF_DMA[DELAY_LEN + (*numPix) * 24 + i + 16] = LOW;
//	  }
//}

void initRainbow(uint8_t* i){


	if((*i)>=(ACCURACY*(COLORNUMBER)))*i=0;
//	if(*i == 0)
//		dir=dir==0?1:0;

	uint8_t currentColorNumber;
	struct RGB color;
	for(uint8_t k=0;k<HEIGHT;++k){
		for(uint8_t j=0;j<WIDTH;++j){
//				currentColorNumber = dir==1?*i+(7-j-k):*i+j+k;
//				currentColorNumber = *i+j+k;
				currentColorNumber = *i+(7-j-k);
//			currentColorNumber = *i+j+k*HEIGHT;
			getColor(&color,&currentColorNumber);
			fillBuffDma(&color,&k,&j);
		}
	}


}

void sendToLed(){
    if(flagStopDma){
    	HAL_TIM_PWM_Start_DMA(&htim2, TIM_CHANNEL_1, (uint32_t*)&BUF_DMA, ARRAY_LEN);
    	flagStopDma = 0;
    }
}

void switchOffAllLeds(){
	if(flagStopDma){
		ws2812_init();
		uint8_t zero = 0;
		ws2812_pixel_rgb_to_buf_dma(&zero, &zero, &zero);
//		for(uint8_t k = 0; k < LED_COUNT; k++)
//			ws2812_pixel_rgb_to_buf_dma(0, 0, 0, k);
		sendToLed();
//		HAL_TIM_PWM_Start_DMA(&htim2, TIM_CHANNEL_1, (uint8_t*)&BUF_DMA, ARRAY_LEN);
//		flagStopDma = 0;
	}
}

void switchOnLeds(uint8_t red, uint8_t green, uint8_t blue){
	if(flagStopDma){
//		ws2812_init();
		ws2812_pixel_rgb_to_buf_dma(&red, &green, &blue);
//		for(uint8_t k = 0; k < LED_COUNT; k++)
//			ws2812_pixel_rgb_to_buf_dma(red, green, blue, k);
//		HAL_TIM_PWM_Start_DMA(&htim2, TIM_CHANNEL_1, (uint8_t*)&BUF_DMA, ARRAY_LEN);
//		flagStopDma = 0;
		sendToLed();
	}
}


void fillBuffDma(struct RGB* color, uint8_t* k,uint8_t* j){
	uint8_t numPix=(*k)*HEIGHT+(*j);
	  for(uint8_t i = 0; i < 8; i++)
	  {
		if (BitIsSet(color->R, (7 - i)))
			BUF_DMA[DELAY_LEN + numPix * 24 + i + 8] = HIGH;
		else
			BUF_DMA[DELAY_LEN + numPix * 24 + i + 8] = LOW;
		if (BitIsSet(color->G, (7 - i)))
		  BUF_DMA[DELAY_LEN + numPix * 24 + i + 0] = HIGH;
		else
		  BUF_DMA[DELAY_LEN + numPix * 24 + i + 0] = LOW;
		if (BitIsSet(color->B, (7 - i)))
		  BUF_DMA[DELAY_LEN + numPix * 24 + i + 16] = HIGH;
		else
		  BUF_DMA[DELAY_LEN + numPix * 24 + i + 16] = LOW;
	  }
}


void getColor(struct RGB* ans,uint8_t* colorNumber){
	uint8_t number = (*colorNumber);
	uint8_t arrayLength = ACCURACY*(COLORNUMBER-1);
	uint8_t leftIndex,rightIndex;
	if(number>=arrayLength){
		if(number-arrayLength>=ACCURACY){
			leftIndex = 0;
			rightIndex = 1;
		}else{
			rightIndex  = 0;
			leftIndex = COLORNUMBER-1;
		}
	}
	else{
		leftIndex = (number)/ACCURACY;
		rightIndex = leftIndex + 1;
	}

	struct RGB* left = (struct RGB*)arr_colors[leftIndex];
	struct RGB* right = (struct RGB*)arr_colors[rightIndex];
	int16_t step[3];
	step[0] = (right->R-left->R)/ACCURACY;
	step[1] = (right->G-left->G)/ACCURACY;
	step[2] = (right->B-left->B)/ACCURACY;

	number =  (number)%ACCURACY;
	ans->R = left->R+step[0] * number;
	ans->G = left->G+step[1] * number;
	ans->B = left->B+step[2] * number;
}




