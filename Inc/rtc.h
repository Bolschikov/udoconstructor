#ifndef RTC_H_
#define RTC_H_

#include "stm32f1xx_hal.h"

#define ADDR_RTC			0xD0
#define ADDR_MIN			0x01
#define ADDR_HOUR			0x02
#define READ_BIT_RTC		0x01


uint8_t RTC_ConvertFromDec(uint8_t c);
uint8_t RTC_ConvertFromBinDec(uint8_t c);
void sendTimeRtc(uint8_t addr, uint8_t time);



#endif /* RTC_H_ */
