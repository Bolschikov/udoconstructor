

#ifndef WS2812_H_
#define WS2812_H_

#include "stm32f1xx_hal.h"
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
//uint16_t BUF_DMA [ARRAY_LEN];

#define DELAY_LEN 80
#define LED_COUNT 16
//#define ARRAY_LEN DELAY_LEN + LED_COUNT*24 + DELAY_LEN
#define ARRAY_LEN DELAY_LEN + LED_COUNT * 24
#define HIGH			75
#define LOW				28
#define RED 			250
#define GREEN			250
#define BLUE			250
#define NO_COLOR		0

#define BitIsSet(reg, bit) (reg & (1 << bit))



struct RGB{
	uint8_t R;
	uint8_t G;
	uint8_t B;
};

#define HEIGHT 4
#define WIDTH 4
#define ACCURACY 32
#define COLORNUMBER 3


void initRainbow(uint8_t* i);
void getColor(struct RGB* ans,uint8_t* colorNumber);
void initRainbow(uint8_t* i);
#endif /* WS2812_H_ */

