/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "rtc.h"
#include "i2c.h"
#include "ht16k33.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ADC1_IN0_photoresis_Pin GPIO_PIN_0
#define ADC1_IN0_photoresis_GPIO_Port GPIOA
#define SPI1_CS_Pin GPIO_PIN_4
#define SPI1_CS_GPIO_Port GPIOA
#define ALARM_Pin GPIO_PIN_12
#define ALARM_GPIO_Port GPIOA
#define TIM2_CH1_COLOR_TO_DMA_Pin GPIO_PIN_15
#define TIM2_CH1_COLOR_TO_DMA_GPIO_Port GPIOA
#define TIM3_CH1_VIBRO_Pin GPIO_PIN_4
#define TIM3_CH1_VIBRO_GPIO_Port GPIOB
#define External_Interrupt_PIR_Pin GPIO_PIN_5
#define External_Interrupt_PIR_GPIO_Port GPIOB
#define External_Interrupt_PIR_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */
#define LENGTHPACKAGE	7
#define RECEIVEBYTES	1

#define PCKG_IS1S2S3S4	6
#define PCKG_IRGBE		5
#define PCKG_IHME		4

#define PIR_LED_MODE	1
#define ALARM_MODE		2
#define LDR_LED_MODE	3

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
