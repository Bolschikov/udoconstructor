/*
 * control_light.h
 *
 *  Created on: Mar 10, 2019
 *      Author: vb
 */

#ifndef CONTROL_LIGHT_H_
#define CONTROL_LIGHT_H_
#include "ws2812.h"

#define R1	100000
#define U	3.3

extern ADC_HandleTypeDef hadc1;




void setNewLvlLight(uint32_t* lastLvl, uint32_t* curLvl);


#endif /* CONTROL_LIGHT_H_ */
