/*
 * ht16k33.h
 *
 *  Created on: Apr 7, 2019
 *      Author: vb
 */

#ifndef HT16K33_H_
#define HT16K33_H_

#define CLEARBYTE		0x00
#define ADDR_HT			0xE0
#define DISPLAY_ON		0x81
#define TURN_ON_OSC		0x21
#define DOT				0x80


void init7seg();
void clear7seg();
void sendTime(uint8_t time, uint8_t item7seg);
void sendNumeric(uint8_t item7seg, uint8_t itemNum);
uint8_t getDecade(uint8_t* time);
uint8_t needUpdate(uint8_t item7seg, uint8_t decade);


#endif /* HT16K33_H_ */
